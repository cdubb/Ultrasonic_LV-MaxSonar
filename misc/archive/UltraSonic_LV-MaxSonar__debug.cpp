/*
  Maxbotix advanced test

  Instructions:
  - Same as simple test

  Filters:
  * NONE (default): No filtering
  * MEDIAN: Take the median of a sample
  * HIGHEST_MODE: Take the mode of a sample. In case more than
  one mode, the highest one is returned
  * LOWEST_MODE: Take the mode of a sample. In case more than
  one mode, the lowest one is returned
  * BEST: Take the mode of a sample. In case more than one
  mode is found, the median is returned
  * SIMPLE: Continue reading until sample_size consecutive readings
  are issued

*/
#include "Maxbotix.h"

/* ************************************************
  Macros
* ************************************************/

/* ************************************************
  Constants
* ************************************************/
static const uint8_t PWM_SENSOR_PIN = 9;
static const uint8_t SERIAL_SENSOR_PIN = 6;
static const uint8_t ANALOG_SENSOR_PIN = A0;

/* ************************************************
  Global variables
* ************************************************/
//Sensor configuration
Maxbotix rangeSensorPW(PWM_SENSOR_PIN, Maxbotix::PW, Maxbotix::LV, Maxbotix::BEST);

/* ************************************************
  Function declaration
* ************************************************/
void printArray(float* array, uint8_t array_size);

/* ************************************************
  Function definitions
* ************************************************/
/*
  Initialization

  Setup method
*/
void setup() {
  Serial.begin(9600);
}

/*
  Main function

  Using PWM and mode filter
*/
void loop() {
  unsigned long start = 0;
  unsigned long currentTime = 0;

  //Serial.println("Reading...");

  //Accumulate time
  currentTime += millis();

  //set start time (TODO: is this necessary?)
  if (start == 0) {
    start = currentTime;
  }

  float cm = rangeSensorPW.getRange();
  //float cm = rangeSensorPW.getSampleMode(true);
  //float inches = rangeSensorPW.toInches(cm);
  Serial.print("(");
  Serial.print(cm);
  Serial.print(",");
  Serial.print(currentTime);
  Serial.print("),");
  //printArray(rangeSensorPW.getSample(), rangeSensorPW.getSampleSize());

  //Serial.println();

  delay(100);
}

/*
  Print array of values
*/
void printArray(float* array, uint8_t array_size) {
  Serial.print("[");
  for (int i = 0; i < array_size; i++) {
    Serial.print(array[i]);
    if (i != array_size - 1) {
      Serial.print(", ");
    }
  }
  Serial.print("]");
}
