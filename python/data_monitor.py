#!/usr/bin/env python
"""
    origin: http://arduino-er.blogspot.com/2015/04/python-to-plot-graph-of-serial-data.html

    Serial port speed: 9600

    To use:
        1. Set the the serial port driver and speed
        2. Start the Arduino and upload an image to the device if needed. Make
           sure the program output format matches the expected input format.
        3. Start this program
"""

import logging
import argparse
import ast
import atexit
import matplotlib.pyplot as plt
import numpy as np
import serial
from drawnow import *


"""
Globals
"""
#Constants
DEFAULT_SERIAL_PORT = '/dev/ttyACM0'
DEFAULT_BAUD_RATE = 9600

#Logger config
LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
LOGGER = logging.getLogger(__name__)

class Data_Monitor(object):
    """Listens on the serial port for data and plots the values in real-time on an xy-plane.
    """

    def __init__(self, port, baud, title):
        self.values = list()
        self.port = port
        self.baud = baud
        self.cnt = 0
        self.serial_conn = serial.Serial(self.port, self.baud)
        self.run_flag = False

        #Plot layout
        self.title = title
        self.y_label = 'Values'
        self.y_legend_label = self.y_label.lower()
        self.y_format_str = 'rx-'
        self.y_min = 0
        self.y_max = 100

        self.y_major_ticks = np.arange(self.y_min, self.y_max , 5)
        self.y_minor_ticks = np.arange(self.y_min, self.y_max , 1)

        #Turn matplotlib interactive mode on
        plt.ion()

    def stop(self):
        self.run_flag = False
        plt.ioff()

    def set_y_axis_range(self, y_min, y_max):
        self.y_min = y_min
        self.y_max = y_max
        self.y_major_ticks = np.arange(self.y_min, self.y_max , 5)
        self.y_minor_ticks = np.arange(self.y_min, self.y_max , 1)

    def plot_values(self):
        plt.title(self.title)
        plt.grid(True)
        plt.ylabel(self.y_label)
        plt.ylim(ymin=self.y_min, ymax=self.y_max)
        #yaxis = plt.axes().yaxis
        #yaxis.set_ticks(self.y_major_ticks)
        #yaxis.set_ticks(self.y_minor_ticks, minor=True)
        plt.plot(self.values, self.y_format_str, label=self.y_legend_label)
        plt.legend(loc='upper right')


    def do_at_exit(self):
        self.serial_conn.close()
        LOGGER.info("Close serial")
        LOGGER.info("serial_conn.isOpen() = " + str(self.serial_conn.isOpen()))

    def run(self):
        LOGGER.info("serial_conn.isOpen() = " + str(self.serial_conn.isOpen()))
        self.run_flag = True

        #pre-load dummy data
        for i in range(0,26):
            self.values.append(0)

        while self.run_flag:
            while (self.serial_conn.inWaiting()==0):
                pass
            LOGGER.debug("readline()")
            value_str = self.serial_conn.readline(500)

            #check if valid value can be casted
            try:
                value = ast.literal_eval(value_str)
                print(value)

                if value < 0:
                    value = 0

                if value <= 1024:
                    self.values.append(value)
                    self.values.pop(0)
                    drawnow(self.plot_values)
                    """
                    if value <= 1024:
                        if value >= 0:
                            self.values.append(value)
                            self.values.pop(0)
                            drawnow(self.plot_values)
                        else:
                            LOGGER.error("Invalid! negative number")

                    """
                else:
                    LOGGER.error("Invalid! too large")
            except ValueError:
                LOGGER.error("Invalid! cannot cast")
            except Exception, ex:
                LOGGER.error(ex)

def main():
    """Main program entry point
    """

    #Command line interface
    parser = argparse.ArgumentParser(description='Listens for data coming over the serial port and plots it in real-time',
                                    epilog='Example: python data_monitor.py -p /dev/ttyACM0 -b 9600')

    parser.add_argument("-p", "--port", type=str, default=DEFAULT_SERIAL_PORT, help="Serial port", required=True)
    parser.add_argument("-b", "--baud", type=int, default=DEFAULT_BAUD_RATE, help="Baud rate", required=True)
    parser.add_argument("-t", "--title", type=str, default="Live data from sensor", help="Name of the plot or graph")
    parser.add_argument("-yl", "--y_axis_label", type=str, default="Values", help="y-axis Label")
    parser.add_argument("-yf", "--y_axis_format_str", type=str, default="rx-", help="y-axis format string")
    parser.add_argument("-ymin", "--y_axis_min", type=int, default="0", help="y-axis minimum range")
    parser.add_argument("-ymax", "--y_axis_max", type=int, default="100", help="y-axis maximum range")


    #Logger
    logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)

    #Process the command line arguements
    args = parser.parse_args()

    #Data monitor
    data_monitor = Data_Monitor(args.port, args.baud, args.title)
    atexit.register(data_monitor.do_at_exit)

    #Setup plot
    data_monitor.title = args.title
    data_monitor.y_label = args.y_axis_label
    data_monitor.y_format_str = args.y_axis_format_str
    data_monitor.set_y_axis_range(args.y_axis_min, args.y_axis_max)

    try:
        data_monitor.run()
    except KeyboardInterrupt:
        #data_monitor.run_flag = False
        data_monitor.stop()
        exit()
    except Exception as e:
        LOGGER.error('data monitor exited with error, caused by Exception')
        LOGGER.error(e)




if __name__ == '__main__':
    main()
