Data Monitor
============

Recommend using _vituralenv_ to run the script.

```
pip install virtualenv
virturalenv venv
```

# Setup the dependencies

Activate virtual env and install the dependenciens
```


## Mac Users

If you are using a Mac, you will need modify the _activate_ and _deactivate_ scripts for the virtual python environment you created. To do that, run the following commands. See (http://stackoverflow.com/questions/21784641/installation-issue-with-matplotlib-python) for more details:

```
echo -e "\necho backend: TkAgg > ~/.matplotlib/matplotlibrc" >> venv/bin/activate
echo -e "\nalias deactivate='deactivate; rm -f ~/.matplotlib/matplotlibrc; unalias deactivate'" >> venv/bin/activate
```

