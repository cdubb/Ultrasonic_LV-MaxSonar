/*
  Maxbotix advanced test

  Instructions:
  - Same as simple test

  Filters:
  * NONE (default): No filtering
  * MEDIAN: Take the median of a sample
  * HIGHEST_MODE: Take the mode of a sample. In case more than
  one mode, the highest one is returned
  * LOWEST_MODE: Take the mode of a sample. In case more than
  one mode, the lowest one is returned
  * BEST: Take the mode of a sample. In case more than one
  mode is found, the median is returned
  * SIMPLE: Continue reading until sample_size consecutive readings
  are issued

*/
#include "Maxbotix.h"

/* ************************************************
  Macros
* ************************************************/

/* ************************************************
  Constants
* ************************************************/
//Pinout
static const uint8_t PWM_SENSOR_PIN = 9;
static const uint8_t SERIAL_SENSOR_PIN = 6;
static const uint8_t ANALOG_SENSOR_PIN = A0;

//Calibration
const uint32_t CALIBRATION_PERIOD_MS = 2000;    //Amount of time the device needs to calibrate itself to detect it's position
const float SENSOR_MOTION_DETECT_CM = 2.0 * 2.54;   //Distance / height to trigger a new measurment; detects if someone is under the sensor
const float SENSOR_ERROR_CM = 0.0;        //Sensor measurment error. TODO: Determin value
const float CALIBRATION_TOLORANCE = 2.54;    //Amount of fluxuation the sensor can have during calibration

/* ************************************************
  Global variables
* ************************************************/
//Sensor configuration
Maxbotix rangeSensorPW(PWM_SENSOR_PIN, Maxbotix::PW, Maxbotix::LV, Maxbotix::BEST, 9);
//Maxbotix rangeSensorPW(PWM_SENSOR_PIN, Maxbotix::PW, Maxbotix::LV, Maxbotix::MEDIAN);

//Don't use filters for this project. They do not work well when the sensor is moving.
//Maxbotix rangeSensorPW(PWM_SENSOR_PIN, Maxbotix::PW, Maxbotix::LV, Maxbotix::NONE);

//UltraSonic
int sensor_position_cm = 0;
bool calibrated = false;      //Flag that marks if the system has detected it's current position
static uint32_t calib_period_counter = 0;
/* ************************************************
  Function declaration
* ************************************************/
void printArray(float* array, uint8_t array_size);
void detect_position();

/* ************************************************
  Function definitions
* ************************************************/
/*
  Determine the current position of the sensor. This is used to initially determine
  the height of the sensor. After a continuous measurment has been read for the
  duration of the calibration period, the sensor position is set and the calibrated
  flag should be set.

  1. reads sensor position
  2. checks if sensor position has changed
  3. if the sensor position (or measurment) hasn't changed, then update the counter
  4. If the counter is greater-than or equal-to the calibration period, then set the
     calibrated flag

*/
void detect_position() {

  uint32_t current_time = millis();
  float current_sensor_pos = rangeSensorPW.getRange();// + SENSOR_ERROR_CM;

  /*
  Serial.println(calib_period_counter);
  Serial.print("current: ");
  Serial.println(current_sensor_pos);
  Serial.print("calibrated: ");
  Serial.println(sensor_position_cm);
  */

  //Sensor reading is constant
  if (current_sensor_pos >= sensor_position_cm - CALIBRATION_TOLORANCE
      &&  current_sensor_pos <= sensor_position_cm + CALIBRATION_TOLORANCE) {

    //Serial.print("elapsed: ");
    //Serial.println((current_time - calib_period_counter));

    if ((calib_period_counter) >= CALIBRATION_PERIOD_MS
        && calib_period_counter != 0) {

      calibrated = true;
      sensor_position_cm = current_sensor_pos;

      Serial.print("Calibrated at: ");
      Serial.print(sensor_position_cm);
      Serial.println(" cm");
    }

    calib_period_counter += current_time - calib_period_counter;

  //Sensor reading changed
  } else {
    sensor_position_cm = current_sensor_pos;
    calib_period_counter = 0;
  }


}



/*
  Initialization

  Setup method
*/
void setup() {
  Serial.begin(9600);

  //calibrated = true;
  //sensor_position_cm = 100;
}

/*
  Main function

  Using PWM and mode filter
*/
void loop() {
  unsigned long start = 0;
  unsigned long currentTime = 0;

  //Accumulate time
  currentTime += millis();

  //set start time (TODO: is this necessary?)
  if (start == 0) {
    start = currentTime;
  }

  if (!calibrated) {
    detect_position();

    //Print zeros during calibration
    Serial.print("Calibrating: ");
    Serial.println(sensor_position_cm);
    //Serial.println(rangeSensorPW.toInches(sensor_position_cm));

    delay(100);
  } else {
    //Read distance from the sensor
    float distance_cm = rangeSensorPW.getRange();
    float height_diff = sensor_position_cm - distance_cm;   //height difference in cm

    Serial.println(height_diff);
    //Serial.println(rangeSensorPW.toInches(height_diff));
    //Serial.println(distance_cm);

    delay(100);
  }

}

/*
  Print array of values
*/
void printArray(float* array, uint8_t array_size) {
  Serial.print("[");
  for (int i = 0; i < array_size; i++) {
    Serial.print(array[i]);
    if (i != array_size - 1) {
      Serial.print(", ");
    }
  }
  Serial.print("]");
}
