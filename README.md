Ultrasonic_LV-MaxSonar
------------------
Ultra sonic sensor Arduino project. Uses a high performance ultrasonic range finder.

This project uses the following libraries:
 * [arduino-Maxbotix](https://github.com/Diaoul/arduino-Maxbotix)


### Directory: misc
__Excluded from build__



### Build Commands

Clean project
```
pio run -e uno -t clean
```

Dump current build environment
```
pio run  -e uno --target=envdump
```

Build (will upload if auto upload is enabled, see _platformio.ini_)
```
pio run -e uno
```

Build and upload
```
pio run -e uno --target=upload
```


### Links

[Using a MaxSonar With an Arduino](http://www.maxbotix.com/articles/085.htm)
Article from OEM about how to wire-up a MaxBotix Inc., MaxSonar sensor to an Arduino.

[MaxSonar Arduino playground](origin: http://playground.arduino.cc/Main/MaxSonar)
Different examples of MaxSonar ultrasonic sensor with the Arduino. Includes examples
reading analog signal, reading PWM, working PWM interface with a mode filter. It also
has other useful information about the sensor.
